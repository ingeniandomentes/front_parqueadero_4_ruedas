import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { ContentComponent } from './components/content/content.component';
import { NotFoundComponent } from './components/notfound/notfound.component';
import { InfoParqueaderoComponent} from './components/info-parqueadero/info-parqueadero.component';
import { CantidadMarcasComponent} from './components/cantidad-marcas/cantidad-marcas.component';
import { DemoComponent} from './components/demo/demo.component';
import { LoggedGuard } from './guards/logged.guard';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {
    path: '',
    canActivateChild: [LoggedGuard],
    children: [
      {path: 'inicio', component: ContentComponent},
      {path: 'info_parqueadero', component: InfoParqueaderoComponent},
      {path: 'cantidad_marcas', component: CantidadMarcasComponent},
      {path: 'demo', component: DemoComponent},
      {path: '**', component: NotFoundComponent, pathMatch: 'full'},
    ]
  },
  {path: '**', component: NotFoundComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
