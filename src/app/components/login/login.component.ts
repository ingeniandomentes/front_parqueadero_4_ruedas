import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from '../../services/login.service';
import {CookieService} from 'ngx-cookie-service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formGroup: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private cookie: CookieService,
    private  formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.email,  Validators.required]],
      password: ['', [Validators.required]]
    });


    if (this.cookie.get('Cookie-User')) {
      this.router.navigate(['/inicio']);
    }
  }

  iniciar_sesion() {

    const temporaryThis = this;
    let working = false;
    this.submitted = true;
    const user = this.formGroup.value;
    const form = this.formGroup;

    $('.login').on('submit', function(e) {
      if (working) return;
      working = true;
      let $this = $(this),
      $state = $this.find('button > .state');
      $this.addClass('loading');
      $state.html('Autenticando');

      const email = user.email;
      const password = user.password;

      if (email.length === 0 || password.length === 0) {
        $this.addClass('error');
        $state.html('¡Correo y/o contraseña no pueden estar vacios!');
        setTimeout(function () {

          $this.removeClass('loading');
          $this.removeClass('error');
          $state.html('Iniciar Sesión');
        }, 2000);
      } else {
        temporaryThis.loginService.login('auth/login', email, password).subscribe(
          result => {
            if (result['access_token']) {
              setTimeout(function() {
                $this.addClass('ok');
                $state.html('¡Bienvenido!');
                setTimeout(function() {
                  temporaryThis.cookie.set('Cookie-User', btoa(result['access_token']));
                  temporaryThis.cookie.set('Name-User', btoa(result['user_name']));
                  temporaryThis.router.navigate(['inicio']);
                }, 1000);
              }, 3000);
            } else {
              $this.addClass('error');
              $state.html('¡No existen coincidencias!');
              setTimeout(function() {
                $this.removeClass('loading');
                $this.removeClass('error');
                $state.html('Iniciar Sesión');
                form.reset();
              }, 2000);
            }
          },
          error => {
            $this.addClass('error');
            $state.html('¡No existen coincidencias!');
            setTimeout(function() {
              $this.removeClass('loading');
              $this.removeClass('error');
              $state.html('Iniciar Sesión');
              form.reset();
            }, 2000);
          }
        );
      }
    });
  }

  isBase64(str) {
    try {
        return btoa(atob(str)) == str;
    } catch (err) {
        return false;
    }
  }

}
