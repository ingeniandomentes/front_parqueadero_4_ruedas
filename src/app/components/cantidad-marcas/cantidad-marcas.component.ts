import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ParqueaderoService } from '../../services/parqueadero.service';

@Component({
  selector: 'app-cantidad-marcas',
  templateUrl: './cantidad-marcas.component.html',
  styleUrls: ['./cantidad-marcas.component.scss']
})
export class CantidadMarcasComponent implements OnInit {

  vehiculos:any;
  tablesVisibility:any = false;

  constructor(
    private parqueaderoService:ParqueaderoService
  ) { }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartLabels = [];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [];

  ngOnInit(): void {

  }

  consultVehicles(){
    this.vehiculos = [];
    this.barChartLabels = [];
    this.barChartData = [];
    this.parqueaderoService.getQuantity('auth/parqueadero/vehiculos').subscribe(
      result =>{
        let datos = [];
        this.vehiculos = result;
        for(let i = 0; i < this.vehiculos.length; i++){
          this.barChartLabels.push(this.vehiculos[i].marca);
          datos.push(this.vehiculos[i].cantidad);
        }
        this.barChartData = [
          {data: datos, label: 'Cantidad Vehiculos'},
        ];
        this.tablesVisibility = true;
      },
      error => {
        console.error(error);
      }
    );
  }

}
