import { Component, OnInit } from '@angular/core';
import { ParqueaderoService } from '../../services/parqueadero.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogParqueaderoComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-info-parqueadero',
  templateUrl: './info-parqueadero.component.html',
  styleUrls: ['./info-parqueadero.component.scss']
})
export class InfoParqueaderoComponent implements OnInit {

  public formGroup: FormGroup;
  public vehiculos:any;
  constructor(
    private parqueaderoService:ParqueaderoService,
    private  formBuilder: FormBuilder,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      marca: [''],
      placa: [''],
      nombre: [''],
      cedula: [''],
    });
  }

  consultVehicles(){
    const data_filters = this.formGroup.value;
    const form = this.formGroup;
    this.parqueaderoService.getVehicles('auth/parqueadero_info', data_filters.marca, data_filters.placa, data_filters.nombre, data_filters.cedula).subscribe(
      result =>{
        console.log(result);
        this.vehiculos = result;
        form.reset();
      },
      error => {
        console.error(error);
        form.reset();
      }
    );
  }

  openDialog(){
    const dialogRef = this.dialog.open(DialogParqueaderoComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.consultVehicles();
    });
  }

}
