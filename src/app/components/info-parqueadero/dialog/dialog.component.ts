import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {CookieService} from 'ngx-cookie-service';
import { ParqueaderoService } from '../../../services/parqueadero.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogParqueaderoComponent implements OnInit {

  localData: any;
  tipoUsuario: any;
  propietarios: any;
  public formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogParqueaderoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cookie: CookieService,
    private parqueaderoService:ParqueaderoService,
    private  formBuilder: FormBuilder,
  ) {
    this.localData = { ...data };

  }
  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      marca: [''],
      modelo: [''],
      placa: [''],
      tipo: [''],
      typeUser: [''],
      nombre: [''],
      cedula: [''],
      telefono: [''],
      tiempo_parqueo: [''],
      id_selected_propietario: [''],
    });
  }

  changeTipoUsuario(e){
    if(e.target.value === "0"){
      this.tipoUsuario = 0;
    } else {
      this.callPropietarios();
      this.tipoUsuario = 1;
    }
  }

  callPropietarios(){
    this.parqueaderoService.getVehicles('auth/parqueadero_info').subscribe(
      result =>{
        this.propietarios = result;
      },
      error => {
        console.error(error);
      }
    );
  }

  saveVehicle(){
    const vehicles = this.formGroup.value;
    console.log(vehicles.id_selected_propietario);
    this.parqueaderoService.saveVehicle('auth/parqueadero', vehicles).subscribe(
      result =>{
        this.closeDialog();
      },
      error => {
        console.error(error);
      }
    );
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
