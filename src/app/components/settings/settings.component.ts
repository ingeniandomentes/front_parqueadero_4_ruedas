import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../services/login.service';
import {CookieService} from 'ngx-cookie-service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private cookie: CookieService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  cerrarSesion(){
    this.loginService.logout('auth/logout').subscribe(
      result => {
        if (result) {
          this.cookie.deleteAll();
          this.router.navigate(['/login']);
        } else {
          this.cookie.deleteAll();
          this.router.navigate(['/login']);
        }
      },
      error => {
        this.cookie.deleteAll();
        this.router.navigate(['/login']);
      }
    );
  }

}
