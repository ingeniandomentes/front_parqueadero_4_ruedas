import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  user_name: any;
  constructor(
    private cookie: CookieService,
  ) { }

  ngOnInit(): void {
    this.user_name = atob(this.cookie.get('Name-User'));
  }

}
