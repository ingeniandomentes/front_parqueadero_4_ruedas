import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private  formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      code: [''],
    });
  }

  generateObject(){
    const formulario = this.formGroup.value;
    let formatArray = [];
    formatArray.push(formulario.code.split(' ,'));
    for(let i = 0; i < formatArray[0].length; i++){
      formatArray[0][i] = formatArray[0][i].replace('"','');
      formatArray[0][i] = formatArray[0][i].replace('[','');
      formatArray[0][i] = formatArray[0][i].replace(']','');
      formatArray[0][i] = formatArray[0][i].split(',');
    }
    console.log('Dado el array:')
    console.log(formatArray);
    let fechas = [];
    for(let i =0; i < formatArray[0].length; i++){
      let position = '';
      let json = {fecha: formatArray[0][i][0]};
      fechas.push(json);
    }

    let fechasClean = {};

    fechas = fechas.filter(function(currentObject) {
        if (currentObject.fecha in fechasClean) {
            return false;
        } else {
            fechasClean[currentObject.fecha] = true;
            return true;
        }
    });

    let jsonFinal = {};
    for(let i = 0; i < fechas.length; i++){
      let position =  fechas[i].fecha;
      jsonFinal[position] = {};
    }

    for(let i =0; i < fechas.length; i++){
      let temp = {}
      for(let j = 0; j < formatArray[0].length; j++ ){
        if(fechas[i].fecha == formatArray[0][j][0]){
          let position1 = formatArray[0][j][0];
          let position2 = formatArray[0][j][1]+"-"+formatArray[0][j][2];
          jsonFinal[position1][position2] = formatArray[0][j][3];
        }
      }
    }
    console.log("Objeto resultante:")
    console.log(jsonFinal);
  }

}
