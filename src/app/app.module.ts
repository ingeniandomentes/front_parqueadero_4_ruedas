import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { ChartsModule } from 'ng2-charts';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/notfound/notfound.component';
import { InfoParqueaderoComponent } from './components/info-parqueadero/info-parqueadero.component';
import { CantidadMarcasComponent } from './components/cantidad-marcas/cantidad-marcas.component';
import { DemoComponent } from './components/demo/demo.component';
import { DialogParqueaderoComponent } from './components/info-parqueadero/dialog/dialog.component';

import {LoginService} from './services/login.service';
import {CookieService} from 'ngx-cookie-service';
import {ParqueaderoService} from './services/parqueadero.service';


@NgModule({
  entryComponents: [
    DialogParqueaderoComponent
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    ContentComponent,
    FooterComponent,
    SettingsComponent,
    LoginComponent,
    NotFoundComponent,
    InfoParqueaderoComponent,
    CantidadMarcasComponent,
    DemoComponent,
    DialogParqueaderoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([]),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    ChartsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    LoginService,
    CookieService,
    ParqueaderoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
