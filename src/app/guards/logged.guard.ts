import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {LoginService} from './../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedGuard implements CanActivate {
  public validate: any;
  constructor(
    private router: Router,
    private cookie: CookieService,
    private LoginService: LoginService) {
  }

  canActivateChild() {
    return this.canActivate();
  }

  canActivate() {
    if (this.cookie.get('Cookie-User')) {
    } else {
      const link = ['/login'];
      this.cookie.deleteAll();
      this.router.navigate(link);
      return false;
    }
    return true;
  }
}
