import { Component } from '@angular/core';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Parqueadero 4 Ruedas';
  login: any = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe(
      (val) => {
        if (val instanceof NavigationEnd) {
          if (window.location.href.split('/')[3] === 'login' || window.location.href.split('/')[3] === '' ) {
            this.login = true;
          } else {
            this.login = false;
          }
        }
    });
  }
}
