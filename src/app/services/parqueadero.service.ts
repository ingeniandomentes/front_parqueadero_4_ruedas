import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})

export class ParqueaderoService {
  public API_URL = environment.API_URL;
  public datosApi: any;
  public token: any;
  public httpOptions: any;
  public validate: any;

  constructor(
    private http: HttpClient,
    private cookie: CookieService
  ) {}

  getVehicles(apiConsult: string, marca:any = '', placa:any = '', nombre:any = '', cedula:any = '' ) {
    this.datosApi = {
      marca,
      placa,
      nombre,
      cedula
    };

    this.token = this.cookie.get('Cookie-User');

    this.httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'Authorization': 'Bearer ' + atob(this.token),
        }
      )
    };
    return this.http.post(this.API_URL + apiConsult , this.datosApi, this.httpOptions);
  }

  getQuantity(apiConsult: string){
    this.token = this.cookie.get('Cookie-User');

    this.httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'Authorization': 'Bearer ' + atob(this.token),
        }
      )
    };

    return this.http.get(this.API_URL + apiConsult, this.httpOptions);
  }

  saveVehicle(apiConsult: string, vehicle ) {

    if(vehicle.typeUser === "0"){
      this.datosApi = {
        "marca": vehicle.marca,
        "modelo":vehicle.modelo,
        "placa": vehicle.placa,
        "tipo": vehicle.tipo,
        "typeUser": vehicle.typeUser,
        "nombre": vehicle.nombre,
        "cedula": vehicle.cedula,
        "telefono": vehicle.telefono,
        "tiempo_parqueo": vehicle.tiempo_parqueo,
      };
    } else {
      this.datosApi = {
        "marca": vehicle.marca,
        "modelo":vehicle.modelo,
        "placa": vehicle.placa,
        "tipo": vehicle.tipo,
        "typeUser": vehicle.typeUser,
        "id_selected_propietario":parseInt(vehicle.id_selected_propietario,10)
      };
    }

    this.token = this.cookie.get('Cookie-User');

    this.httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'Authorization': 'Bearer ' + atob(this.token),
        }
      )
    };
    return this.http.post(this.API_URL + apiConsult , this.datosApi, this.httpOptions);
  }
}
