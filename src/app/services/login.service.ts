import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  public API_URL = environment.API_URL;
  public datosApi: any;
  public token: any;
  public httpOptions: any;
  public validate: any;

  constructor(
    private http: HttpClient,
    private cookie: CookieService
  ) {}

  login(apiConsult: string, email: any = '', password: any = '' ) {
    this.datosApi = {
      email,
      password,
    };
    this.httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest'
        }
      )
    };
    return this.http.post(this.API_URL + apiConsult , this.datosApi, this.httpOptions);
  }

  logout(apiConsult: string) {
    this.token = this.cookie.get('Cookie-User');

    this.httpOptions = {
      headers: new HttpHeaders({
          'Authorization': 'Bearer ' + atob(this.token),
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest'
        }
      )
    };
    return this.http.get(this.API_URL + apiConsult ,this.httpOptions);
  }
}
